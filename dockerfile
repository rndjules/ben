FROM python:latest

COPY basic-flask-app ./app
WORKDIR app

RUN python3 -m venv venv
RUN . venv/bin/activate
RUN pip install -U Flask

CMD ["python", "routes.py"]

